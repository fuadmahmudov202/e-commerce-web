

<input type="hidden" id="deleteModalStoreInputId">
<div class="modal-body">
    Are u sure to delete this product?
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" onclick="closeDeleteModal()" data-bs-dismiss="modal">Close</button>
    <button type="button" onclick="deleteStore($('#deleteModalStoreInputId').val())" class="btn btn-primary">Delete</button>
</div>
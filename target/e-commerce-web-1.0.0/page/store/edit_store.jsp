<script>
    function closeModal(){
        $('#editStoreModalId').modal('hide');
    }
    $(function() {
        loadProductType('editProductTypeId', false);
        $('#editProductTypeId').val(${product.productTypeId});
        loadProductId(${product.productTypeId}, 'editProductId', false);
        $('#editProductId').val(${product.productId})


    });


</script>

<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Edit Store</h5>

</div>
<input type="hidden" id="editStoreId" value="${product.id}">
<div class="modal-body">
    <div class="mb-3">
        <select class="form-control" aria-label="product type" onchange="loadProductId($('#editProductTypeId').val(),'editProductId')" id="editProductTypeId"></select>
    </div>

    <div class="mb-3">
        <select class="form-control" aria-label="product" id="editProductId"></select>
    </div>

    <div class="mb-3">
        <input type="number" class="form-control" id="editQuantityId" value="${product.quantity}" placeholder="Quantity">
    </div>


</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" onclick="closeModal()" data-bs-dismiss="modal">Close</button>
    <button type="button" onclick="editStoreModal($('#editStoreId').val(),$('#editQuantityId').val())" class="btn btn-primary">Save</button>
</div>
function loadData(url,method,id, isAsync){
    $.ajax({
        url: url,
        method: method,
        async: isAsync,
        success: function (result){
            $('#'+id).html(result)
        },
        error: function (result){}
    })
}


function loadStore(){
    console.log("in loadStore method..");
    $.ajax({
        url: 'store',
        method: 'GET',
        success: function(result){
            $('#storeTableId').html(result)
        },
        error: function (result){

        }
    });

}

function openStoreModal(){
    $('#storeModalId').modal('show');
}

function loadProductType(id,isAsync){
    var async = true;
    if (isAsync != undefined && isAsync != null){
        async = isAsync;
    }

    loadData('product-type','GET',id,async);
}

function loadProductId(productTypeId,productId, isAsync){
    //alert("secilmis id"+productTypeId);
    var async = false;
    if (isAsync != undefined && isAsync != null){
        async = isAsync;
    }

    loadData('products?productTypeId='+productTypeId,'GET',productId,async);
}

function closeStoreModal(){
    $('#storeModalId').modal('hide');
}

function saveStore(){
    var data = {
        'productId' : $('#productId').val(),
        'quantityId': $('#quantityId').val(),
    };
    console.log( $('#productId').val());
    console.log( $('#quantityId').val());
    const url = 'store';

    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'html',
        success: function(){
            //modal hide
            closeStoreModal();
            loadStore();
        },
        error:function (){
            console.log("product exist");
            alert("product exist");
            productExistError();
        }

    });
}

function productExistError(){
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong! :product already exist',
        footer: '<a href>Why do I have this issue?</a>'
    })
}

function openEditModal(id){

    $.ajax({
        url: 'edit-store?id='+id,
        method: 'GET',
        success: function(data){
            $('#editStoreModalContentId').html(data);
            $('#editStoreModalId').modal('show');
        },
        error: function (result){

        }
    });
}
function editStoreModal(id,quantity){
    var data = {
        'id' : id,
        'quantityId' :quantity,
    };
    console.log(id);
    console.log(quantity);
    const url ='edit-store'
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'html',
        success: function(data){
            closeEditModal();
            loadStore();
            /*$('#editStoreModalContentId').html(data);
            $('#editStoreModalId').modal('show');*/
        },
        error: function (result){

        }
    });
}
function closeEditModal(){
    $('#editStoreModalId').modal('hide');
}

function openDeleteModal(id){
    $('#deleteStoreModal').modal('show');
    $('#deleteModalStoreInputId').val(id)
}

function closeDeleteModal(){
    $('#deleteStoreModal').modal('hide');
}

function deleteStore(id){
    var data = {
        'deleteModalStoreInputId' : id,
    };

    const url = 'delete-store';
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'html',
        success: function(){
            //modal hide
            closeDeleteModal();
            loadStore();
        },
        error:function (){
        }
    });
}
function resetPassword(username,password){
    var data = {
        'username' : username,
        'password' : password,
    };

    const url = 'reset-password';
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'html',
        success: function(){
            //modal hide

        },
        error:function (){
        }
    });

}

function registerUser(username,password,confirmPassword,email,userType){

    var data = {
        'regUsername' : username,
        'password' : password,
        'confirm_password' : confirmPassword,
        'regEmail' : email,
        'regUserType' : userType,
    };
    console.log(username);
    console.log(password);
    console.log(confirmPassword);
    console.log(email);
    console.log(userType);

    const url = 'register';
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: 'html',
        success: function(){
            Swal.success({
                icon: 'success',
                title: 'Done',
                text: 'Successfully added',
            })
        },
        error:function (){
        }
    });
}





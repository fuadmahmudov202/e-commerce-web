<%--
  Created by IntelliJ IDEA.
  User: FUAD's laptop
  Date: 2/14/2021
  Time: 6:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>


<link href="resources/template/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/template/css/login.css" rel="stylesheet">
<script src="resources/template/js/jquery.min.js"></script>
<script src="resources/template/js/bootstrap.bundle.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

<html>
<head>
    <title>Register Page</title>
</head>
<body>
<div class="sidenav">
    <div class="login-main-text">
        <h1>E-COMMERCE<br> Register Page</h1>
    </div>
</div>
<div class="main">
    <div class="col-md-6 col-sm-12">
        <div class="login-form" style="margin-top: 40%">
            <form method="post">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" id="regUsername"  name="regUsername" placeholder="Username" required>
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                </div>
                <div class="form-group">
                    <label>Confirm Password</label>
                    <input type="password" id="confirm_password" class="form-control" name="confirm_password" placeholder="Confirm Password" required>
                </div>
                <span id='message'></span>
                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" id="regEmail" name="regEmail" placeholder="Email" required>
                </div>
                <div class="form-group">
                    <select class="form-control" id="regUserType" name="regUserType">
                        <option value="1">User</option>
                        <option value="2">Admin</option>
                    </select>
                </div>
                <a href="login.jsp" class="btn btn-black">Back</a>
                <button type="submit" onclick="registerUser($('#regUsername').val(),$('#password').val(),
                                                            $('#confirm_password').val(),$('#regEmail').val(),
                                                            $('#regUserType').val())"
                        class="btn btn-secondary" >Register
                </button>
            </form>

        </div>

    </div>
</div>

</body>
<script>
    $('#confirm_password').on('keyup', function () {
        if ($(this).val() == $('#password').val()) {
            $('#message').html('matching').css('color', 'green');
        } else $('#message').html('not matching').css('color', 'red');
    });
</script>
</html>

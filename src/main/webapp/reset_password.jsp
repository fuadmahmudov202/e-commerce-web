<%--
  Created by IntelliJ IDEA.
  User: FUAD's laptop
  Date: 2/14/2021
  Time: 12:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<body>
<div id="login">
    <h3 class="text-center text-white pt-5">Login form</h3>
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="" method="post">
                        <h3 class="text-center text-info">Reset Password</h3>
                        <div class="form-group">
                            <label for="usernameId" class="text-info">Username:</label><br>
                            <input type="text" name="username" id="usernameId" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="passwordId" class="text-info">Password:</label><br>
                            <input type="password" name="password" id="passwordId" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="confirmPassword" class="text-info">Password:</label><br>
                            <input type="password" name="confirmPassword" id="confirmPassword" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" onclick="resetPassword($('#usernameId').val(),$('#passwordId').val())" class="btn btn-info btn-md" value="reset">
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<!--Section: Block Content-->
<%--<html>
<head>
    <title>Reset password</title>
</head>
<body>
hello
<form method="post">
    Username:<input type="text" id="usernameId" name="username"><br>
    Password:<input type="password" id="passwordId" name="password"><br>
    Password:<input type="password" name="confirmPassword"><br>
    <button type="submit" onclick="resetPassword($('#usernameId').val(),$('#passwordId').val())" class="btn btn-black">Reset</button>
</form>

</body>
</html>--%>


<%--
  Created by IntelliJ IDEA.
  User: FUAD's laptop
  Date: 1/29/2021
  Time: 8:48 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html lang="en">
<style >
    i:hover {
          cursor: pointer
      }

</style>
<body>
<table class="table table-striped">

    <h2 >Store</h2>
    <tr>
        <th>ID</th>
        <th>PRODUCT_NAME</th>
        <th>QUANTITY</th>
        <th>INSERT DATE</th>
        <th>STATUS</th>
        <th></th>
        <th></th>

    </tr>

    <c:forEach items="${storeList}" var="s">
        <tr>
            <td>${s.id}</td>
            <td>${s.productName}</td>
            <td>${s.quantity}</td>
            <td>${s.insertDateFormatted}</td>
            <td>${s.status}</td>
            <td><i onclick="openEditModal(${s.id})" class="fa fa-pencil-square-o" aria-hidden="true" style="color: green" ></i> </td>
            <td><i onclick="openDeleteModal(${s.id})" class="fa fa-trash-o" style="color: red"></i> </td>
        </tr>
    </c:forEach>


</table>

</body>
</html>

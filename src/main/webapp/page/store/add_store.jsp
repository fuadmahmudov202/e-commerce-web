<%--<script>
    function closeModal(){
        $('#storeModalId').modal('hide');
    }

</script>--%>

<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Add Store</h5>

</div>
<div class="modal-body">
    <div class="mb-3">
    <select class="form-control" aria-label="product type" onchange="loadProductId($('#productTypeId').val(),'productId')" id="productTypeId"></select>
    </div>

    <div class="mb-3">
    <select class="form-control" aria-label="product" id="productId"></select>
    </div>

    <div class="mb-3">
        <input type="number" class="form-control" id="quantityId" placeholder="Quantity">
    </div>


</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" onclick="closeStoreModal()" data-bs-dismiss="modal">Close</button>
    <button type="button" onclick="saveStore()" class="btn btn-primary">Save</button>
</div>
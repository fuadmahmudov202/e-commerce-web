<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link href="resources/template/css/bootstrap.min.css" rel="stylesheet">
<link href="resources/template/css/login.css" rel="stylesheet" id="bootstrap-css">
<script src="resources/template/js/jquery.min.js"></script>
<script src="resources/template/js/bootstrap.bundle.min.js"></script>

<!------ Include the above in your HEAD tag ---------->

<div class="sidenav">
    <div class="login-main-text">
        <h2>Application<br> Login Page</h2>
        <p>Login or register from here to access.</p>
    </div>
</div>
<div class="main">
    <div class="col-md-6 col-sm-12">
        <div class="login-form">
            <span style="color: red">${errorMessage}</span>
            <c:if test="${param.logout eq true}">
                <span style="color: green">You have been logged out successfully</span>
            </c:if>

            <form action="login" method="post">
                <div class="form-group">
                    <label>User Name</label>
                    <input type="text" class="form-control" value="${username}" name="username" placeholder="User Name">
                </div>
                <div class="form-group">
                    <label>Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Password">
                </div>
                <button type="submit" class="btn btn-black">Login</button>
                <a href="register" class="btn btn-secondary">Register</a><br><br>
                <a class="pull-right text-muted" href="forgotPassword" id="forgotID"><small>Forgot your password?</small></a>
            </form>
        </div>
    </div>
</div>

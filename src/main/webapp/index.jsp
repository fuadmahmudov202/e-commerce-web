
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Simple Sidebar - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="resources/template/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="resources/template/css/simple-sidebar.css" rel="stylesheet">
    <%--change to local--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">



</head>

<body>


<div class="d-flex" id="wrapper">

    <!-- Sidebar -->
    <div class="bg-light border-right" id="sidebar-wrapper">
        <c:import url="page/items/menu.jsp"></c:import>

    </div>
    <!-- /#sidebar-wrapper -->

    <!-- Page Content -->
    <div id="page-content-wrapper">



        <div class="card">
            <div class="card-header">

                <button type="button" onclick="openStoreModal()" class="btn btn-primary">Add Product</button>
                <a type="button" class="btn btn-primary" style="float: right; margin-right: 15px" href="logout" >Log out</a>
                <span style="float: right; margin-right: 45px; color: red">${sessionScope.userLogin.username}</span>

            </div>
            <div class="card-body">
                <div class="container-fluid">

                    <div id="storeTableId"></div>
                </div>
            </div>
        </div>


    </div>
    <!-- /#page-content-wrapper -->

</div>
<!-- /#wrapper -->

<!-- Bootstrap core JavaScript -->
<script src="resources/template/js/jquery.min.js"></script>
<script src="resources/template/js/bootstrap.bundle.min.js"></script>
<script src="resources/js/main.js"></script>
<script src="resources/template/js/sweetalert2.all.min.js"></script>



<!-- Menu Toggle Script -->
<script>
    $(function(){
        loadStore();
        loadProductType('productTypeId');
        loadProductType('editProductTypeId');
    })


    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>

</body>

<script>

</script>
<!-- Modal -->
<div class="modal fade" id="storeModalId" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <c:import url="page/store/add_store.jsp"></c:import>
        </div>
    </div>
</div>

<div class="modal fade" id="editStoreModalId" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" id="editStoreModalContentId">
            //load data
        </div>
    </div>
</div>


<div class="modal fade bd-example-modal-sm" id="deleteStoreModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" id="deleteStoreModalId" style="background-color: #bd2130">
            <c:import url="page/store/delete_store.jsp"></c:import>
        </div>
    </div>
</div>

</html>
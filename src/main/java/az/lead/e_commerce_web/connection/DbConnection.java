package az.lead.e_commerce_web.connection;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;
import java.sql.Connection;

public class DbConnection {
    private DbConnection(){}


    public static Connection getConnection(){
        try {
            Context initContext = new InitialContext();
            Context encContext = (Context) initContext.lookup("java:comp/env");
            DataSource ds = (DataSource) encContext.lookup("jdbc/ecommerce-db");
            Connection connection =ds.getConnection();
            connection.setAutoCommit(false);
            return connection;
        }catch (Exception e){
                throw new IllegalStateException("exception happened while connected to database");
        }
    }
}

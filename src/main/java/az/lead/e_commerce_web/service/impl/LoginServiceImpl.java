package az.lead.e_commerce_web.service.impl;

import az.lead.e_commerce_web.exception.LoginException;
import az.lead.e_commerce_web.model.UserLogin;
import az.lead.e_commerce_web.repository.LoginRepo;
import az.lead.e_commerce_web.repository.impl.LoginRepoImpl;
import az.lead.e_commerce_web.service.LoginService;
import az.lead.e_commerce_web.utils.AuthenticationUtils;
import az.lead.e_commerce_web.utils.MailSender;
import az.lead.e_commerce_web.validator.LoginValidator;

public class LoginServiceImpl implements LoginService {
    private final LoginRepo loginRepo = new LoginRepoImpl();
//    @Override
//    public UserLogin login(String username, String password) {
//        LoginValidator.validate(username,password);
//        UserLogin userLogin= loginRepo.findByUsername(username.trim())
//                .orElseThrow(()-> new LoginException("invalid username or password"));
//        if (!AuthenticationUtils.checkPassword(userLogin.getPassword(),password.trim()))
//            throw new LoginException("invalid username or password");
//        return userLogin;
//    }

    @Override
    public UserLogin login(String username, String password) {
        LoginValidator.validate(username,password);
        UserLogin userLogin= loginRepo.findByUsername(username.trim())
                .orElseThrow(()-> new LoginException("invalid username"));
        if (!AuthenticationUtils.checkPassword(userLogin.getPassword(),password.trim()))
            throw new LoginException("invalid password");
        return userLogin;
    }

    @Override
    public void forgotPassword(String email) {
        String mail = loginRepo.findMail(email);
        System.out.println(mail);
        if (mail == null) {
            throw new LoginException("user not found");
        }
        MailSender.sendMail(mail, "forgot Password", "http://localhost:8080/e-commerce-web/reset-password");
    }

    @Override
    public void updatePasswordByUsername(String username, String password) {
        loginRepo.updatePasswordByUsername(username,password);
    }

    @Override
    public void saveUser(UserLogin userLogin) {
        loginRepo.saveUser(userLogin);
    }
}

package az.lead.e_commerce_web.service.impl;

import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.ProductType;
import az.lead.e_commerce_web.repository.ProductRepo;
import az.lead.e_commerce_web.repository.impl.ProductRepoImpl;
import az.lead.e_commerce_web.service.ProductService;

import java.util.List;

public class ProductServiceImpl implements ProductService {
    private ProductRepo productRepo = new ProductRepoImpl();
    @Override
    public Product save(Product product) {
        return productRepo.save(product);
    }

    @Override
    public Product findProductById(Long id) {
        return productRepo.findProductById(id);
    }

    @Override
    public List<ProductType> findAll() {
        return productRepo.findAll();
    }

    @Override
    public List<Product> getProductByProductId(Long id) {
        return productRepo.getProductByProductId(id);
    }
}

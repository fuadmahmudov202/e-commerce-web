package az.lead.e_commerce_web.service.impl;

import az.lead.e_commerce_web.model.EditProduct;
import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.Store;
import az.lead.e_commerce_web.repository.ProductRepo;
import az.lead.e_commerce_web.repository.StoreRepo;
import az.lead.e_commerce_web.repository.impl.ProductRepoImpl;
import az.lead.e_commerce_web.repository.impl.StoreRepoImpl;
import az.lead.e_commerce_web.service.StoreService;

import java.util.List;

public class StoreServiceImpl implements StoreService {
    StoreRepo storeRepo = new StoreRepoImpl();
    @Override
    public List<Store> findAll() {
        return storeRepo.findAll();
    }

    @Override
    public Product addProductToStore(Product product) {
            if (product==null)
                throw new IllegalStateException("product is null");
        return storeRepo.addProductToStore(product);
    }

    @Override
    public EditProduct findById(Long id) {
        EditProduct product = storeRepo.findById(id);
        if (product==null)
            throw new IllegalStateException("store not found");

        return product;
    }

    @Override
    public void deleteProductFromStore(Long productId) {
        storeRepo.deleteProductFromStore(productId);
    }

    @Override
    public void editStoreByProductId(Long productId, Double quantity) {
        storeRepo.editStoreByProductId(productId,quantity);
    }
}

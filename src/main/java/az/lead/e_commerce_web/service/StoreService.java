package az.lead.e_commerce_web.service;

import az.lead.e_commerce_web.model.EditProduct;
import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.Store;

import java.util.List;

public interface StoreService {
    List<Store> findAll();
    Product addProductToStore(Product product);

    EditProduct findById(Long id);

    void deleteProductFromStore(Long productId);

    void editStoreByProductId(Long productId, Double quantity);
}

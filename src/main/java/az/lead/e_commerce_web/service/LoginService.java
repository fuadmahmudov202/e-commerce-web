package az.lead.e_commerce_web.service;

import az.lead.e_commerce_web.model.UserLogin;

public interface LoginService {
    UserLogin login(String username,String password);

    void forgotPassword(String email);

    void updatePasswordByUsername(String username, String password);

    void saveUser(UserLogin userLogin);
}

package az.lead.e_commerce_web.model;

public class Unit extends Base{
private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Unit(String name) {
        this.name = name;
    }

    public Unit(Long id) {
        super.id = id;
    }
}

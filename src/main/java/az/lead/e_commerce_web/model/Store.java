package az.lead.e_commerce_web.model;

import az.lead.e_commerce_web.constats.AppConstants;
import az.lead.e_commerce_web.enums.Status;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Store extends Base {
    private String productName;
    private Double quantity;
    private Integer isActive;
    private LocalDateTime insertDate;
    private String insertDateFormatted;
    private Status status;

    public Store() {
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public Status getStatus() {
        return Status.getStatus(isActive);
    }

    public String getInsertDateFormatted() {
        return insertDate.format(DateTimeFormatter.ofPattern(AppConstants.DATE_TIME_FORMAT));
    }
}

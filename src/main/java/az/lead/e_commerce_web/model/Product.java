package az.lead.e_commerce_web.model;

public class Product extends Base {

    private ProductType productType;
    private Unit unit;
    private String name;
    private Double price;
    private Double quantity;

    public Product( Long id) {
        this.id = id;

    }
    public Product( Long id,Double quantity) {
        this.id = id;
        this.quantity=quantity;
    }
    public Product( ProductType productType, Unit unit, String name, Double price) {
        this.productType = productType;
        this.unit = unit;
        this.name = name;
        this.price = price;
    }

    public Product() {

    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", productType=" + productType +
                ", unit=" + unit +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}

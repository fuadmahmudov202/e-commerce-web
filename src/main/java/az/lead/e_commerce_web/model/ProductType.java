package az.lead.e_commerce_web.model;

public class ProductType extends Base{

    private String name;

    public ProductType(Long id) {
        this.id = id;
    }

    public ProductType(Long id,String name) {
        super.id= id;
        this.name = name;
    }

    public ProductType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




}

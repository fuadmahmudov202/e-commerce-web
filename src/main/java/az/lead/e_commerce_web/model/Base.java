package az.lead.e_commerce_web.model;

import java.io.Serializable;

public class Base implements Serializable {
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}

package az.lead.e_commerce_web.model;

public class EditProduct extends Base{
    private Long productId;
    private Long productTypeId;
    private Double quantity;

    public EditProduct(Long productId,Double quantity){
        this.productId = productId;
        this.quantity = quantity;
    }
    public EditProduct() {
    }

    public EditProduct(Long id,Long productId, Long productTypeId, Double quantity) {
        super.id =id;
        this.productId = productId;
        this.productTypeId = productTypeId;
        this.quantity = quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Long getProductTypeId() {
        return productTypeId;
    }

    public void setProductTypeId(Long productTypeId) {
        this.productTypeId = productTypeId;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }
}

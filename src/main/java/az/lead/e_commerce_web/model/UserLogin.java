package az.lead.e_commerce_web.model;

import az.lead.e_commerce_web.enums.UserType;

import java.io.Serializable;

public class UserLogin extends Base implements Serializable {
    private int userType;
    private UserType userTypeEnum;
    private String username;
    private String password;
    private String email;

    public UserLogin() {
    }

    public UserLogin(Long id, int userType, String username, String password) {
        super.id = id;
        this.userType = userType;
        this.username = username;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserType() {
        return userType;
    }

    public void setUserType(int userType) {
        this.userType = userType;
    }

    public UserType getUserTypeEnum() {
        return UserType.getStatus(userType);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

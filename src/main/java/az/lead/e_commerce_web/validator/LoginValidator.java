package az.lead.e_commerce_web.validator;

import az.lead.e_commerce_web.exception.LoginException;

import java.util.Objects;

public final class LoginValidator {
    private LoginValidator(){

    }

    public static void validate(String username, String password) {
        if (Objects.isNull(username) || username.trim().isEmpty())
            throw new LoginException("username is empty");

        if (Objects.isNull(password) || password.trim().isEmpty())
            throw new LoginException("password is empty");
    }
}

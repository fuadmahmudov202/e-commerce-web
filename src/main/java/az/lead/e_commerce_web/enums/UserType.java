package az.lead.e_commerce_web.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserType {
    USER(1),
    ADMIN(2);

    private final int value;

    public static final Map<Integer,UserType> VALUE_MAP= new HashMap<>();
    static {
        for (UserType status:values()) {
            VALUE_MAP.put(status.value,status);
        }
    }

    private UserType(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }

    public static UserType getStatus(Integer status){
        return VALUE_MAP.get(status);
    }
}

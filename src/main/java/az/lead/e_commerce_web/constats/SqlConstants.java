package az.lead.e_commerce_web.constats;

public final class SqlConstants {
    public static final String ADD_PRODUCT = "insert into product(product_type_id,unit_id,name,price)" +
            "values(?,?,?,?)";
    public static final String PRODUCT_BY_ID = "SELECT pro.name,pro.price,pt.name,unit.name FROM product pro join product_type pt on pro.product_type_id = pt.id join unit on pro.unit_id = unit.id where pro.id=?";
    public static final String FIND_ALL_STORE_PRODUCT = "SELECT s.id, p.name as product_name, s.quantity, s.is_active, s.insert_date \n" +
            "FROM store s, product p \n" +
            "WHERE s.product_id = p.id and s.is_active=1\n" +
            "ORDER BY insert_date desc;";
    public static final String ALL_PRODUCT_TYPE = "select id , name from product_type where is_active =1";
    public static final String ALL_PRODUCT_BY_PRODUCT_TYPE = "SELECT id,name FROM product where product_type_id=?";

//    public static final String CHECK_STORE_BY_PRODUCT_ID ="UPDATE store SET is_active=1 WHERE product_id=?";
    public static final String CHECK_STORE_BY_PRODUCT_ID ="select id from store WHERE is_active=1 and product_id=?";

    public static final String ADD_PRODUCT_TO_STORE = "INSERT INTO store(product_id,quantity,is_active) VALUES (?,?,1);";


    public static final String FIND_STORE_BY_ID = "select s.id ,product_id,p.product_type_id,quantity from store s\n" +
            "inner join product p on s.product_id =p.id where s.is_active=1 and s.id=?";

    public static final String DELETE_PRODUCT_FROM_STORE_BY_ID = "update store set is_active=0 where id=?";
    public static final String EDIT_QUANTITY_BY_PRODUCT_ID = "update store set update_date=now(), quantity=? where id=?";
    public static final String FIND_BY_USERNAME = "select id,user_type,username,password from user_login where username=? and status=1 and user_type=2";
    public static final String FIND_MAIL ="select email from user where email=?";
    public static final String UPDATE_PASSWORD_BY_USERNAME ="update user_login set password=? where username=?";
    public static final String SAVE_USER_LOGIN ="INSERT INTO user_login (username, password, user_type) VALUES (?,?,?)";
    public static final String SAVE_USER_MAIL="insert into user(user_login_id,email) values(?,?)";
}
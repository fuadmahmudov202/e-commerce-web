package az.lead.e_commerce_web.constats;

public class AppConstants {
    private AppConstants(){}

    public static final String DATE_TIME_FORMAT ="dd.MM.yyyy HH:ss";
}

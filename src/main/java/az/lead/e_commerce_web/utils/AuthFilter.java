package az.lead.e_commerce_web.utils;

import az.lead.e_commerce_web.model.UserLogin;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
@WebFilter(urlPatterns = "/*")
public class AuthFilter  implements Filter {
    public static final String SESSION_LOGIN_ATTRIBUTE= "userLogin";
    public static final String RESOURCES_PATH= "/resources";
    public static final String [] ALLOWED_URL_LIST= {"/login","/register","/forgotPassword","/reset-password"};
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
            System.out.println(req.getRequestURI());
            int contextPathLength = Objects.isNull(req.getContextPath()) ? 0 : req.getContextPath().length();
            //get servlet url without context path
            String servletPath = req.getRequestURI().substring(contextPathLength);
            //check if url is resources(css, js) don't check it
            //goto next filter
            if (servletPath.startsWith(RESOURCES_PATH)){
                //goto next filter
                chain.doFilter(req, resp);
                return;
            }
            Optional<String> allowedUrl = Arrays.stream(ALLOWED_URL_LIST)
                    .filter(url-> url.equalsIgnoreCase(servletPath))
                    .findAny();
            //check if url is allowed don't check it
            //goto next filter
            if (allowedUrl.isPresent()){
                //goto next filter
                chain.doFilter(req, resp);
                return;
            }
            //don't create new session if session doesn't exists
            HttpSession session = req.getSession(false);
            if (Objects.isNull(session)){
                // if there is not any session then user don't have successful login
                // because we create only new session when user login attempt is successful
                resp.sendRedirect("login");
                return;
            }
            UserLogin sessionUser = (UserLogin)session.getAttribute(SESSION_LOGIN_ATTRIBUTE);
            if (Objects.isNull(sessionUser)) {
                //same comment as line 47, 48
                resp.sendRedirect("login");
                return;
            }
            //if everything is goto next filter
            chain.doFilter(req, resp);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

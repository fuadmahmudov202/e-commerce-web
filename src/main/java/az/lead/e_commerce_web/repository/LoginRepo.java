package az.lead.e_commerce_web.repository;

import az.lead.e_commerce_web.model.UserLogin;

import java.util.Optional;

public interface LoginRepo {
    Optional<UserLogin> findByUsername(String username);

    String findMail(String email);

    void updatePasswordByUsername(String username, String password);

    void saveUser(UserLogin userLogin);
}

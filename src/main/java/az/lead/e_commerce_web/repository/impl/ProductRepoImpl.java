package az.lead.e_commerce_web.repository.impl;

import az.lead.e_commerce_web.connection.DbConnection;
import az.lead.e_commerce_web.constats.SqlConstants;
import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.ProductType;;
import az.lead.e_commerce_web.model.Unit;
import az.lead.e_commerce_web.repository.ProductRepo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductRepoImpl implements ProductRepo {

    @Override
    public Product save(Product product) {
        String sql = SqlConstants.ADD_PRODUCT;
        try (Connection connection=DbConnection.getConnection();){
              PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setLong(1,product.getProductType().getId());
                ps.setLong(2,product.getUnit().getId());
                ps.setString(3,product.getName());
                ps.setDouble(4,product.getPrice());
                int exist= ps.executeUpdate();
                connection.commit();
                if (exist>0){
                    ResultSet resultSet = ps.getGeneratedKeys();
                    if (resultSet.next())
                        product.setId(resultSet.getLong(1));
                }

                return product;
        } catch (SQLException e) {
            throw new IllegalStateException("exception happened while product added to db");
        }

    }
    @Override
    public Product findProductById(Long id){

        String sql =SqlConstants.PRODUCT_BY_ID;
        try (Connection connection = DbConnection.getConnection();){
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1,id);

            ResultSet rs = ps.executeQuery();
            if (rs.next()){
                Product product = new Product();
                Unit unit = new Unit(rs.getString("unit.name"));
                ProductType productType = new ProductType(rs.getString("pt.name"));
                product.setProductType(productType);
                product.setUnit(unit);
                product.setName(rs.getString("pro.name"));
                product.setPrice(rs.getDouble("pro.price"));

                return product;
            }
            return  null;

        } catch (SQLException throwables) {

            throwables.printStackTrace();
            throw new IllegalStateException("exception happened while show product");
        }

    }

    @Override
    public List<ProductType> findAll() {
        String sql = SqlConstants.ALL_PRODUCT_TYPE;
        try (Connection connection = DbConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery();) {

            List<ProductType> productTypeList = new ArrayList<>();
            while (resultSet.next()) {
                ProductType productType= new ProductType(
                        resultSet.getLong("id"),
                        resultSet.getString("name"));

                productTypeList.add(productType);

            }
            return productTypeList;

        } catch (SQLException e) {
            throw new IllegalStateException("exception happened while get data from database");
        }
    }

    @Override
    public List<Product> getProductByProductId(Long id) {
        String sql =SqlConstants.ALL_PRODUCT_BY_PRODUCT_TYPE;
        try (Connection connection = DbConnection.getConnection();){
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1,id);

            ResultSet rs = ps.executeQuery();
            List<Product> productList = new ArrayList<>();
            while (rs.next()) {
                Product product = new Product();
                product.setId(rs.getLong("id"));
                product.setName(rs.getString("name"));

                productList.add(product);
            }
            return productList;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while show product");
        }
    }

}

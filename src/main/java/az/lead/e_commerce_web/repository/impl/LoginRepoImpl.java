package az.lead.e_commerce_web.repository.impl;

import az.lead.e_commerce_web.connection.DbConnection;
import az.lead.e_commerce_web.constats.SqlConstants;
import az.lead.e_commerce_web.model.UserLogin;
import az.lead.e_commerce_web.repository.LoginRepo;

import java.sql.*;
import java.util.Optional;


public class LoginRepoImpl implements LoginRepo {
    @Override
    public Optional<UserLogin> findByUsername(String username) {
        String sql = SqlConstants.FIND_BY_USERNAME;
        try (Connection connection = DbConnection.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1,username);
            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                UserLogin userLogin = new UserLogin(
                        (rs.getLong("id")),
                        rs.getInt("user_type"),
                        rs.getString("username"),
                        rs.getString("password"));


                return Optional.of(userLogin);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while getting UserLogin info from  db!");
        }
        return Optional.empty();
    }

    @Override
    public String findMail(String email) {
        String sql =SqlConstants.FIND_MAIL;

        try (Connection conn = DbConnection.getConnection();
             PreparedStatement ps = conn.prepareStatement(sql);) {
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                return rs.getString("email");

            }
            return null;

        } catch (SQLException ex) {
            throw new IllegalStateException("system error", ex);
        }
    }

    @Override
    public void updatePasswordByUsername(String username, String password) {
        String sql = SqlConstants.UPDATE_PASSWORD_BY_USERNAME;

        try (Connection connection = DbConnection.getConnection()){
            PreparedStatement ps =connection.prepareStatement(sql);
            ps.setString(1,password);
            ps.setString(2,username);
            int executedRow = ps.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while delete product");
        }
    }

    @Override
    public void saveUser(UserLogin userLogin) {
        String sql = SqlConstants.SAVE_USER_LOGIN;
        String saveUserSql =SqlConstants.SAVE_USER_MAIL;

        try (Connection connection = DbConnection.getConnection()) {
            PreparedStatement ps = connection.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
            PreparedStatement ps2 = connection.prepareStatement(saveUserSql);
            ps.setString(1,userLogin.getUsername());
            ps.setString(2,userLogin.getPassword());
            ps.setInt(3,userLogin.getUserType());
            int executedRow = ps.executeUpdate();
            if (executedRow>0){
                ResultSet resultSet = ps.getGeneratedKeys();
                if (resultSet.next()) {
                    ps2.setLong(1, resultSet.getLong(1));
                }

            }
            ps2.setString(2,userLogin.getEmail());
            ps2.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while adding product to db");
        }
    }


}



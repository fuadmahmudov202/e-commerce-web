package az.lead.e_commerce_web.repository.impl;

import az.lead.e_commerce_web.connection.DbConnection;
import az.lead.e_commerce_web.constats.SqlConstants;
import az.lead.e_commerce_web.model.EditProduct;
import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.Store;
import az.lead.e_commerce_web.repository.StoreRepo;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StoreRepoImpl implements StoreRepo {

    @Override
    public List<Store> findAll() {
        String sql = SqlConstants.FIND_ALL_STORE_PRODUCT;
        try (Connection connection = DbConnection.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(sql);
             ResultSet resultSet = preparedStatement.executeQuery()) {

            List<Store> storeList = new ArrayList<>();
            while (resultSet.next()) {
                Store store = new Store();
                store.setId(resultSet.getLong("id"));
                store.setProductName(resultSet.getString("product_name"));
                store.setQuantity(resultSet.getDouble("quantity"));
                store.setIsActive(resultSet.getInt("is_active"));
                store.setInsertDate(resultSet.getTimestamp("insert_date").toLocalDateTime());

                storeList.add(store);

            }
            return storeList;

        } catch (SQLException e) {
            throw new IllegalStateException("exception happened while get data from database");
        }
    }

    @Override
    public Product addProductToStore(Product product) {
        String checkSql= SqlConstants.CHECK_STORE_BY_PRODUCT_ID;
        String sql = SqlConstants.ADD_PRODUCT_TO_STORE;
        try (Connection connection=DbConnection.getConnection()){
            PreparedStatement checkPs = connection.prepareStatement(checkSql);
            checkPs.setLong(1,product.getId());
            ResultSet resultSet = checkPs.executeQuery();
//            int executedRow = checkPs.executeUpdate();
            if (resultSet.next())
                throw new IllegalStateException("product already exist");

                PreparedStatement ps = connection.prepareStatement(sql);
                ps.setLong(1,product.getId());
                ps.setDouble(2,product.getQuantity());
                ps.executeUpdate();
                connection.commit();

            return product;
        } catch (SQLException e) {
            throw new IllegalStateException("exception happened while product added to db");
        }

    }

    @Override
    public EditProduct findById(Long id) {
        String sql = SqlConstants.FIND_STORE_BY_ID;
        try (Connection connection = DbConnection.getConnection()){
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1,id);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                EditProduct product = new EditProduct(
                rs.getLong("id"),
                rs.getLong("product_id"),
                rs.getLong("product_type_id"),
                rs.getDouble("quantity")
                );
                return product;
            }
                return null;

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while show product");
        }

    }

    @Override
    public void deleteProductFromStore(Long productId) {
        String sql = SqlConstants.DELETE_PRODUCT_FROM_STORE_BY_ID;
        try (Connection connection = DbConnection.getConnection()){
            PreparedStatement ps =connection.prepareStatement(sql);
            ps.setLong(1,productId);
            int executedRow = ps.executeUpdate();
            connection.commit();


        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while delete product");
        }

    }

    @Override
    public void editStoreByProductId(Long productId, Double quantity) {
        String sql = SqlConstants.EDIT_QUANTITY_BY_PRODUCT_ID;

        try (Connection connection = DbConnection.getConnection()){
            PreparedStatement ps =connection.prepareStatement(sql);
            ps.setDouble(1,quantity);
            ps.setLong(2,productId);
            int executedRow = ps.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new IllegalStateException("exception happened while delete product");
        }
    }
}

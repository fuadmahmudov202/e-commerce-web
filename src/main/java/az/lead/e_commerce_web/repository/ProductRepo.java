package az.lead.e_commerce_web.repository;

import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.ProductType;

import java.util.List;

public interface ProductRepo {
    Product save(Product product);

    Product findProductById(Long id);

    List<ProductType> findAll();

    List<Product> getProductByProductId(Long id);
}

package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.model.ProductType;
import az.lead.e_commerce_web.service.ProductService;
import az.lead.e_commerce_web.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/product-type"})
public class ProductTypeController extends HttpServlet {
    private final ProductService productService = new ProductServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<ProductType> productTypeList = productService.findAll();
        req.setAttribute("productTypeList",productTypeList);
        req.getRequestDispatcher("page/product/product_type.jsp").forward(req,resp);
    }

}

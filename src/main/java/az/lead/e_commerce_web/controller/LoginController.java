package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.exception.LoginException;
import az.lead.e_commerce_web.model.UserLogin;
import az.lead.e_commerce_web.service.LoginService;
import az.lead.e_commerce_web.service.impl.LoginServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(urlPatterns = {"/login"})
public class LoginController extends HttpServlet {
    private LoginService loginService = new LoginServiceImpl();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username= req.getParameter("username");
        String password= req.getParameter("password");
        try {

            UserLogin userLogin = loginService.login(username,password);
            HttpSession session =req.getSession();
            session.setAttribute("userLogin",userLogin);
            if (session==null)
                throw new LoginException("Something went wrong");
            resp.sendRedirect("index");
        }catch (LoginException ex){
            req.setAttribute("username",username);
            req.setAttribute("errorMessage",ex.getMessage());
            req.getRequestDispatcher("login.jsp").forward(req,resp);

        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.addHeader("Pragma", "no-cache");
        resp.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        resp.addHeader("Cache-Control", "pre-check=0, post-check=0");
        resp.setDateHeader("Expires", 0);
        req.getRequestDispatcher("login.jsp").forward(req,resp);
    }
}

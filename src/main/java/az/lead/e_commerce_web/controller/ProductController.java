package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.ProductType;
import az.lead.e_commerce_web.model.Unit;
import az.lead.e_commerce_web.service.ProductService;
import az.lead.e_commerce_web.service.impl.ProductServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = {"/products"})
public class ProductController extends HttpServlet {
    private ProductService productService = new ProductServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id=Long.valueOf(req.getParameter("productTypeId"));

        List<Product> productList = productService.getProductByProductId(id);
        req.setAttribute("productList",productList);
        req.getRequestDispatcher("page/product/product.jsp").forward(req,resp);
    }
}

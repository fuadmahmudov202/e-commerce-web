package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.ProductType;
import az.lead.e_commerce_web.model.Unit;
import az.lead.e_commerce_web.service.ProductService;
import az.lead.e_commerce_web.service.impl.ProductServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(urlPatterns = {"/product"})
public class UserController extends HttpServlet {

    private ProductService productService = new ProductServiceImpl();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String productType = req.getParameter("productType");
        String unit = req.getParameter("unit");
        String name = req.getParameter("name");
        String price= req.getParameter("price");

        Product product = new Product(new ProductType(Long.valueOf(productType)),
                          new Unit(Long.valueOf(unit)),
                          name,
                          Double.valueOf(price)  );
         product = productService.save(product);
         //req.setAttribute("product",product);
         resp.sendRedirect("product?id="+product.getId());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String id = req.getParameter("id");
        Product product =productService.findProductById(Long.valueOf(id));
        req.setAttribute("product",product);
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("store.jsp");
        requestDispatcher.forward(req,resp);

    }
}

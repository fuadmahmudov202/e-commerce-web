package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.exception.LoginException;
import az.lead.e_commerce_web.service.LoginService;
import az.lead.e_commerce_web.service.impl.LoginServiceImpl;
import az.lead.e_commerce_web.utils.MailSender;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/forgotPassword"})
public class ForgotPasswordController extends HttpServlet {
    private final LoginService loginService = new LoginServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action= req.getServletPath();
        if (action!=null)
            if (action.equalsIgnoreCase("/forgotPassword")) {
                req.getRequestDispatcher("forgot_password.jsp").forward(req, resp);
            }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email= req.getParameter("email");
        System.out.println(email);
        if (email==null)
            throw new LoginException("email is empty");

        loginService.forgotPassword(email);
    }
}

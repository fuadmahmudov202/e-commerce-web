package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.model.EditProduct;
import az.lead.e_commerce_web.model.Product;
import az.lead.e_commerce_web.model.Store;
import az.lead.e_commerce_web.service.StoreService;
import az.lead.e_commerce_web.service.impl.StoreServiceImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns ={"/store","/edit-store","/delete-store"})
public class StoreController extends HttpServlet {
    private final StoreService storeService = new StoreServiceImpl();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action= req.getServletPath();
        if (action!=null)
            if (action.equalsIgnoreCase("/store")){
                List< Store > storeList= storeService.findAll();
                req.setAttribute("storeList",storeList);
                RequestDispatcher requestDispatcher =req.getRequestDispatcher("page/store/store.jsp");
                requestDispatcher.forward(req,resp);
            }else if (action.equalsIgnoreCase("/edit-store")){
                String id = req.getParameter("id");
                if (id==null)
                    throw new IllegalStateException("store id is null");

                EditProduct product = storeService.findById(Long.valueOf(id));
                req.setAttribute("product",product);
                req.getRequestDispatcher("page/store/edit_store.jsp")
                            .forward(req,resp);
            }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String action= req.getServletPath();
        if (action!=null)
            if (action.equalsIgnoreCase("/store")) {
                String productReq= req.getParameter("productId");
                String quantity= req.getParameter("quantityId");
                if (productReq==null || quantity==null)
                    throw new IllegalStateException("parameters values are invalid");

                Long productId =Long.valueOf(productReq);
                Double quantityId=Double.valueOf(quantity);

                Product product = new Product(productId,quantityId);
                storeService.addProductToStore(product);

            }else if (action.equalsIgnoreCase("/delete-store")){
                String deletedProductId = req.getParameter("deleteModalStoreInputId");
                Long productId = Long.valueOf(deletedProductId);
                Product product = new Product(productId);
                storeService.deleteProductFromStore(productId);
            }else if (action.equalsIgnoreCase("/edit-store")){
                String editProductId= req.getParameter("id");
                if (editProductId==null)
                    throw new IllegalStateException("store id is null");

                String quantityId= req.getParameter("quantityId");
                Long productId = Long.valueOf(editProductId);
                Double quantity = Double.valueOf(quantityId);
                EditProduct product =new EditProduct(productId,quantity);
                storeService.editStoreByProductId(productId,quantity);
            }


    }
}

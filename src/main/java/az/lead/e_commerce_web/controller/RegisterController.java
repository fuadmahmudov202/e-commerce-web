package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.exception.LoginException;
import az.lead.e_commerce_web.model.UserLogin;
import az.lead.e_commerce_web.service.LoginService;
import az.lead.e_commerce_web.service.impl.LoginServiceImpl;
import az.lead.e_commerce_web.utils.PasswordEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = {"/register"})
public class RegisterController extends HttpServlet {
    LoginService loginService = new LoginServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("register.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String username = req.getParameter("regUsername");
        String password = req.getParameter("password");
        String confirmPassword = req.getParameter("confirm_password");
        String email = req.getParameter("regEmail");
        String userType = req.getParameter("regUserType");
        if (!password.equals(confirmPassword))
            throw new LoginException("passwords not matching");

        UserLogin userLogin = new UserLogin();
        userLogin.setUsername(username);
        userLogin.setPassword(PasswordEncoder.getMd5Password(password));
        userLogin.setEmail(email);
        userLogin.setUserType(Integer.valueOf(userType));

        req.setAttribute("user", userLogin);
        loginService.saveUser(userLogin);
        String regMessage = "User has been successfully saved";
        req.setAttribute("regMessage",regMessage);
        req.getRequestDispatcher("login.jsp").forward(req, resp);
    }
}


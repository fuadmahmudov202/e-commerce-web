package az.lead.e_commerce_web.controller;

import az.lead.e_commerce_web.service.LoginService;
import az.lead.e_commerce_web.service.impl.LoginServiceImpl;
import az.lead.e_commerce_web.utils.PasswordEncoder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns ={"/reset-password"} )
public class ResetPasswordController extends HttpServlet {
    private final LoginService loginService = new LoginServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

            req.getRequestDispatcher("reset_password.jsp").forward(req, resp);

        }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String username = req.getParameter("username");
        String password = req.getParameter("password");
        loginService.updatePasswordByUsername(username,PasswordEncoder.getMd5Password(password));
        resp.sendRedirect("login.jsp");
    }
}
